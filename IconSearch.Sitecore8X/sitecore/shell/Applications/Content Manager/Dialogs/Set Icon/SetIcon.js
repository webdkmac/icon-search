function scClick(element, evt) {
    evt = scForm.lastEvent ? scForm.lastEvent : evt;
    var icon = evt.srcElement ? evt.srcElement : evt.target;

    var edit = scForm.browser.getControl("IconFile");

    if (!icon) {
        return;
    }

    var src = null;
    if (icon.tagName && icon.tagName.toLowerCase() == "img" && icon.className == "scRecentIcon") {
        src = icon.src;
    }
    else {
        src = scForm.browser.isIE ? icon["sc_path"] : icon.getAttribute("sc_path");
    }

    if (src == null) {
        return;
    }

    var n = src.indexOf("/~/icon/");

    if (n >= 0) {
        src = src.substr(n + 8);
    }
    else if (src.substr(0, 32) == "/sitecore/shell/themes/standard/") {
        src = src.substr(32);
    }

    n = src.indexOf("/temp/IconCache/");
    if (n >= 0) {
        src = src.substr(n + 16);
    }

    if (src.substr(src.length - 5, 5) == ".aspx") {
        src = src.substr(0, src.length - 5);
    }

    edit.value = src;
}

function scChange(element, evt) {
    var element = scForm.browser.getControl("Selector");

    var id = element.options[element.selectedIndex].value + "List";

    var list = scForm.browser.getControl("List");

    var childNodes = list.childNodes;

    for (var n = 0; n < childNodes.length; n++) {
        var element = childNodes[n];

        element.style.display = (element.id == id ? "" : "none");
    }

    scUpdateControls();
}

function scUpdateControls() {
    if (!scForm.browser.isIE) {
        scForm.browser.initializeFixsizeElements();
    }
}

function searchButtonChange(el, event) {
    var element = scForm.browser.getControl("Selector");

    var searchText = event.srcElement ? event.srcElement.value : event.target.value;

    if (!searchText) {
        return;
    }

    var id = "AllList";

    var list = scForm.browser.getControl("List");

    var childNodes = list.childNodes;


    var foundIcons = [];
    for (var n = 0; n < childNodes.length; n++) {
        var element = childNodes[n];

        if (element.id == id) {
            var areaImage = element.getElementsByTagName('img')[0];
            element.style.display = "";
            areaImage.style.display = "none";
            element.getElementsByTagName('map')[0].style.display = "none";

            var wrapper = document.getElementById('search-result-wrapper');

            if (wrapper) {
                element.removeChild(wrapper);
            }

            var allIcons = element.getElementsByTagName('map')[0].getElementsByTagName('area');

            for (var i = 0; i < allIcons.length; i++) {
                var currentIcon = allIcons[i];

                if (currentIcon.getAttribute("sc_path").indexOf(searchText) > -1) {
                    foundIcons.push(currentIcon);
                }
            }

            if (foundIcons.length > 0) {

                var wrapper = document.createElement("div");
                wrapper.style.maxWidth = "60%";
                wrapper.id = "search-result-wrapper";
                for (var i = 0; i < foundIcons.length; i++) {
                    var coordinates = foundIcons[i].getAttribute("coords");


                    var coordinatesArray = coordinates.split(",");

                    var x = ((coordinatesArray[0] - 4) / 40) * -40 - 4;
                    var y = ((coordinatesArray[1] - 4) / 40) * -40 - 4;

                    var tempDiv = document.createElement("div");

                    tempDiv.setAttribute("sc_path", foundIcons[i].getAttribute("sc_path"));
                    tempDiv.style.width = "32px";
                    tempDiv.style.height = "32px";
                    tempDiv.style.display = "inline-block";
                    tempDiv.style.margin = "2px";
                    tempDiv.style.background = "url('" + areaImage.src + "') " + x + "px " + y + "px";
                    tempDiv.style.cursor = "pointer";
                    tempDiv.onclick = updateDiv;
                    wrapper.appendChild(tempDiv);
                }

                element.appendChild(wrapper);

            }

        }
        else {
            element.style.display = "none";
        }

    }

    scUpdateControls();
}

function updateDiv() {

    var wrapper = document.getElementById('search-result-wrapper');
    var allIcons = wrapper.getElementsByTagName('div');

    for (var i = 0; i < allIcons.length; i++) {
        allIcons[i].style.border.width = "";
        allIcons[i].style.border = ""
    }

    this.style.border.width = "1px";
    this.style.border = "solid #add8e6";
    scClick(this, '');


}